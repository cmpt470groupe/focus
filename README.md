# Focus

Welcome to Focus!

## Coding Style

Python: [PEP 8](http://python.org/dev/peps/pep-0008/)

JavaScript: [Idiomatic.js](https://github.com/rwaldron/idiomatic.js/) with one exception: no whitespace before the first and after the last argument of a function.

## Static Analysis

Python:

* [Pylint](http://www.pylint.org/)

* [autopep8](https://pypi.python.org/pypi/autopep8/1.0.3)

## Getting started with development

1. Install the required [Python](https://www.python.org/) version (we're targeting version 3.3 or 3.4 at this point).

2. Install [pip](https://pip.pypa.io/en/latest/installing.html), the Python package manager.

3. Install [virtualenvwrapper](http://virtualenvwrapper.readthedocs.org/) (optional but recommended), a tool for managing isolated environments for Python projects, and create a virtualenv for the project:

    `mkvirtualenv --python /usr/bin/python3.3 focus`

5. Install the project's dependencies:

    `pip install -r requirements.txt`

6. Run the tests:

    `python focus/manage.py test`

7. Have Django create the database tables, and create an admin user if prompted:

    `python focus/manage.py syncdb`

8. Run the development server:

    `python focus/manage.py runserver`

9. While the development server is running, you should be able to browse the site at the URL printed by the previous command. The development server auto-reloads when you make changes to the code.

## Deploying as a Docker container

[Docker](http://www.docker.com/) provides an easy way to deploy (or develop) Focus. We've included a `Dockerfile` that you can run Focus in Docker.

1. Follow the documentation to [install Docker](https://docs.docker.com/installation/) on your server or workstation.

2. Build the container image for Focus:

    `sudo docker build -t="yourusername/focus:latest" .`

3. Run the container:

    `sudo docker run -t -i -p 8000:80 yourusername/focus:latest`

4. View the site using the exposed port (eg. `http://localhost:8000`)

You can stop the container at any time with ctrl+c. If you update your code, run the build command again to rebuild the image before running it again. Because of Docker's caching, this should be very fast after the first time.

For debugging purposes, you can attach to a TTY on a container via:

`sudo docker run -t -i -p 8000:80 yourusername/focus:latest /bin/bash`
