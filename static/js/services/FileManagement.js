/* fileManagement.js

Description:File management service and factory for the angular JS single page application

Team 1 Group D

Created by: Derek Ho, 2014-07-03

Programmers: Derek Ho, Kelvin Lau

Changes:
2014-07-03  Added features to call the REST API for uploading
2014-07-05  Added features to call the REST API to delete files
2014-07-05  Added validation of file in size and file type by extension


Known bugs:
None
*/

(function() {

  this.factory('FileManager', function(Restangular) {
    var endpoint = Restangular.one('file_management/upload');

    function uploadFile(file, courseId) {
      var form = new FormData()
        , filesize = file.size
        , fname = file.name
        , exts = /(\.jpg|\.jpeg|\.bmp|\.gif|\.png|\.pdf|\.txt|\.mp4|\.avi|\.mov|\.mpg|\.mpeg)$/i;

      //Front end validaiton of file size and file type.
      if (!exts.exec(fname)) {
        alert("This file type is not supported");
      } else if (filesize > 41943040) { //40 Megabyte limit
        alert("File size is too large. Files must be less than 40MB in size")
      } else {
        form.append('file', file);
        form.append('filesize', file.size);
        form.append('filename', file.name);
        form.append('filetype', file.type);
        form.append('course_id', courseId);

        return endpoint.withHttpConfig({
          transformRequest: angular.identity
        }).customPOST(form, '', undefined, {
          'Content-Type': undefined
        });
      }

    }

    function getFiles(courseId) {
      return endpoint.getList(courseId);
    }

    function deleteFile(file) {
      var endpoint = Restangular.all('file_management/delete');

      return endpoint.customDELETE(file.id);
    }

    return {
      uploadFile: uploadFile,
      getFiles: getFiles,
      deleteFile: deleteFile
    };
  });

}).call(angular.module('services'));
