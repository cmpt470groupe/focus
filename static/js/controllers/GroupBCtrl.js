(function() {

  this.controller('GroupBCtrl', function($scope, GroupB) {
    console.log('GroupBCtrl');
    $scope.hello = 'Hello from GroupBCtrl';

    GroupB.testCall().then(function(data) {
      if (data.test === "success") {
        $scope.testCall = true;
      }
    });
  });

}).call(angular.module('controllers'));
