(function() {

  this.controller('GroupCCtrl', function($scope, GroupC) {
    console.log('GroupCCtrl');
    $scope.hello = 'Hello from GroupCCtrl';

    GroupC.testCall().then(function(data) {
      if (data.test === "success") {
        $scope.testCall = true;
      }
    });
  });

}).call(angular.module('controllers'));
