"""Account management views.

views.py
Worked on by: Tom Dryer (Team 1, Group A)
"""

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.conf import settings
from django.db.models import Q
from django.middleware import csrf
from rest_framework.exceptions import (ParseError, AuthenticationFailed,
                                       PermissionDenied)
from rest_framework.permissions import BasePermission, IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView
import logging

from accounts.models import (Course, get_instructor_group, gen_password,
                             notify_user)
from accounts.serializers import (
    LoginSerializer, EmailSerializer, NewCourseSerializer, CourseSerializer,
    ModifyCourseSerializer, ChangeSessionSerializer
)


logger = logging.getLogger(__name__)


def deserialize_or_fail(request, serializer_cls):
    """Deserialize object from the request data, or fail with Bad Request."""
    serializer = serializer_cls(data=request.DATA)
    if serializer.is_valid():
        return serializer.data
    else:
        raise ParseError(detail=serializer.errors)


class IsAuthenticatedOrPost(BasePermission):
    """The request is authenticated as a user, or is a POST request."""

    def has_permission(self, request, view):
        return (request.method == 'POST' or
                (request.user and request.user.is_authenticated()))


class SessionView(APIView):

    # Allow unauthenticated access to POST for logging in
    permission_classes = (IsAuthenticatedOrPost,)

    def get(self, request):
        """Show information associated with the current login session.

        Returns 200 if successful.

        Returns 403 if not logged in.
        """
        is_instructor = request.user.groups.filter(name='Instructors').exists()
        return Response({
            'email': request.user.username,
            'id': request.user.id,
            'is_admin': request.user.is_superuser,
            'is_instructor': is_instructor,
            'name': request.user.first_name,
        }, status=200)

    def post(self, request):
        """Create a new login session.

        Takes parameters `email` and `password`.

        Returns 200 if successful, and creates a new session.

        Returns 400 if request is invalid.

        Returns 403 if login credentials are incorrect.
        """
        credentials = deserialize_or_fail(request, LoginSerializer)
        user = authenticate(username=credentials['email'],
                            password=credentials['password'])
        if user is None:
            raise AuthenticationFailed()
        login(request, user)
        is_instructor = request.user.groups.filter(name='Instructors').exists()
        return Response({
            'email': request.user.username,
            'id': request.user.id,
            'is_admin': request.user.is_superuser,
            'is_instructor': is_instructor,
            'name': request.user.first_name,
            # Including the sessionid and csrftoken here is a kludge to work
            # around PhoneGap being cookie-challenged.
            'sessionid': request.session._session_key,
            'csrftoken': csrf.get_token(request),
        }, status=200)

    def delete(self, request):
        """Delete the current session.

        Clients should call this endpoint to securely log out.

        Returns 204 if successful, and deletes the session.

        Returns 403 if not logged in.
        """
        logout(request)
        return Response(status=204)

    def patch(self, request):
        """Modify the session to change the password or profile name.

        Returns 204 if successful, and changes the password and/or name.

        Returns 400 if request is invalid, old password is incorrect, new
        password is too short, or name is too long.

        Returns 403 if not logged in.
        """
        data = deserialize_or_fail(request, ChangeSessionSerializer)
        if (data['old_password'] is not None and
                data['new_password'] is not None):
            if not request.user.check_password(data['old_password']):
                raise ParseError('Old password does not match.')
            if len(data['new_password']) < settings.MIN_PASSWD_LEN:
                raise ParseError('New password is too short.')
            request.user.set_password(data['new_password'])
            request.user.save()
            return Response(status=204)
        if data['name'] is not None:
            request.user.first_name = data['name']
            request.user.save()
            return Response(status=204)
        raise ParseError('Must change name or password.')


class InstructorView(APIView):

    permission_classes = (IsAdminUser,)

    def post(self, request):
        """Create a new instructor.

        Takes parameter `email`.

        Returns 201 if successful and creates the new instructor.

        Returns 403 if not logged in or logged in user is not an admin.

        Returns 400 if request is invalid, or instructor with that email
        already exists.
        """
        email = deserialize_or_fail(request, EmailSerializer)['email']
        if User.objects.filter(email=email).first() is not None:
            return Response({
                'detail': 'Instructor with this email already exists.',
            }, status=400)
        password = gen_password()
        user = User.objects.create_user(email, email, password)
        get_instructor_group().user_set.add(user)
        notify_user(user, 'Focus: Instructor Account Created',
                    'Welcome to Focus. Your login credentials are {} and {}.'
                    .format(user.email, password))
        return Response(status=201)


class CoursesView(APIView):

    def get(self, request):
        """View your list of courses.

        Everyone can see courses they are enrolled in. Instructors can also see
        courses they are instructing.

        Returns 403 if not logged in.

        Returns 200 with list of courses if successful.
        """
        # Due to the way ManyToMany fields work, we need distinct() here to
        # remove duplicate results.
        courses = Course.objects.filter(
            Q(instructor=request.user) |
            Q(enrolled_users=request.user)
        ).distinct()
        # hacky way of getting the emails into the serializer
        for course in courses:
            course.student_emails = (user.email for user
                                     in course.enrolled_users.all())
        serializer = CourseSerializer(courses, many=True)
        return Response(serializer.data)

    def post(self, request):
        """Create a new course.

        Takes parameters `name` and `student_emails`.

        Returns 201 if successful and creates the new course.

        Returns 403 if not logged in or logged in user is not an instructor.

        Returns 400 if request is invalid, or course with that name already
        exists.
        """
        if not request.user.groups.filter(name='Instructors').exists():
            raise PermissionDenied()
        data = deserialize_or_fail(request, NewCourseSerializer)
        if Course.objects.filter(name=data['name']).first() is not None:
            return Response({
                'detail': 'Course with this name already exists.',
            }, status=400)
        Course.objects.create_course(data['name'], request.user,
                                     data['student_emails'])
        return Response(status=201)

class CourseView(APIView):

    def get(self, request, course_id):
        """View a course.

        Returns 200 with course if successful.

        Returns 403 if not logged in, or the user is not enrolled in the course
        nor its instructor.

        Returns 404 if course with the given ID does not exist.
        """
        try:
            course = Course.objects.get(pk=course_id)
        except Course.DoesNotExist:
            return Response(status=404)
        if (request.user != course.instructor
            and request.user not in course.enrolled_users.all()):
            raise PermissionDenied()
        # hacky way of getting the emails into the serializer
        course.student_emails = [user.email for user
                                 in course.enrolled_users.all()]
        return Response(CourseSerializer(course).data, status=200)

    def patch(self, request, course_id):
        """Modify a course, allowing students to be added or removed.

        Returns 204 if successful and modifies the course.

        Returns 400 if request is invalid.

        Returns 403 if not logged in as the course's instructor.

        Returns 404 if course with the given ID does not exist.
        """
        try:
            course = Course.objects.get(pk=course_id)
        except Course.DoesNotExist:
            return Response(status=404)
        if course.instructor != request.user:
            raise PermissionDenied()
        modified_course = deserialize_or_fail(request, ModifyCourseSerializer)
        enrolled_users = [Course.objects.get_or_create_user(email=email)
                          for email in modified_course['student_emails']]
        course.enrolled_users.clear()
        course.enrolled_users.add(*enrolled_users)
        return Response(status=204)
