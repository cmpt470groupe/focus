""" urls.py

Description: Url configuration for group_d upload file features

Team 1 Group D 

Created by: Kelvin Lau, 2014-06-17

Programmers: Derek Ho, Kelvin Lau

Changes: 
2014-07-05 Fixed URL details for class view instead of individual views

Known bugs: 
None
"""
from django.conf import urls
from rest_framework import routers

from file_management import views

urlpatterns = urls.patterns(
    '',
    urls.url(r'^upload$', views.FileUploadView.as_view()),
    urls.url(r'^upload/(?P<course_id>\d+)$', views.GetFileUpload.as_view()),
    urls.url(r'^delete/(?P<id>\d+)$', views.FileDeleteView.as_view()),
)

